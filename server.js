const express = require('express');
const app = express();
const http = require('http').Server(app);
const io = require('socket.io')(http);

app.set('view engine', 'pug');
app.use(express.static('public'));

app.get('/', (req, res) => {
    res.render('index');
});

io.on('connection', function(socket){
    console.log('a user connected');

    socket.on('disconnect', function(){
        console.log('user disconnected');
    });

    socket.on('clickMessage', function(msg){
        console.log('message'+msg);
        socket.broadcast.emit('clickMessage',msg);
    });
});

const PORT = process.env.PORT || 8080;
http.listen(PORT);