(() => {
  let stage = new createjs.Stage("mainCanvas");

  let socket = io();

  // クリックイベントの追加
  stage.addEventListener('click', onClickEvent);
  let cEffects = [];

  let circle = new createjs.Shape();
  circle.graphics.beginFill("DeepSkyBlue").drawCircle(0, 0, 20);

  let blurFilter = new createjs.BlurFilter(40, 40, 1);
  circle.filters = [blurFilter];

  var bounds = blurFilter.getBounds();
  circle.cache(-50 + bounds.x, -50 + bounds.y, 100 + bounds.width, 100 + bounds.height);

  stage.addChild(circle);
  stage.update();

  createjs.Ticker.addEventListener('tick', handleTick);

  function handleTick(event) {
    // マウス座標を取得する
    let mx = stage.mouseX;
    let my = stage.mouseY;
    // シェイプをマウスに追随させる
    circle.x = mx;
    circle.y = my;

    cEffects.forEach(cEffect => {
      if (cEffect.alpha > 0) {
        let alpha100 = cEffect.alpha * 100;
        alpha100 -= 6;
        cEffect.alpha = alpha100 / 100;

        let scaleMultiple = 1.2;
        cEffect.scaleX *= scaleMultiple;
        cEffect.scaleY *= scaleMultiple;
      } else {
        stage.removeChild(cEffect);
      }
    });

    cEffects = cEffects.filter((cEffect) => {
      return cEffect.alpha > 0;
    });

    // 画面を更新する
    stage.update();
  }

  function onClickEvent(event) {

    let cEffect = createClickEffect(event.stageX, event.stageY);

    stage.addChild(cEffect);
    cEffects.push(cEffect);

    stage.update();

    let clickEvent = {
      x: event.stageX,
      y: event.stageY
    };

    socket.emit('clickMessage', JSON.stringify(clickEvent));
  }

  socket.on('clickMessage', (msg) => {

    let ocEvent = JSON.parse(msg);
    console.log(ocEvent);

    let oEffect = createClickEffect(ocEvent.x, ocEvent.y, 'Pink');
    stage.addChild(oEffect);
    cEffects.push(oEffect);
  });

  /**
   * 
   * @param {int} x 
   * @param {int} y 
   * @param {string} color 
   */
  function createClickEffect(x, y, color) {

    if (!color) {
      color = 'SkyBlue';
    }

    let effect = new createjs.Shape();
    effect.graphics.beginStroke(color).drawCircle(0, 0, 10);
    effect.alpha = 1;
    effect.scaleX = 1;
    effect.scaleY = 1;

    effect.x = x;
    effect.y = y;

    return effect;
  }

})();